#!/bin/python3
from turtle import *
import random

speed(10)
penup()
goto(-140, 120)

for stap in range(15):
    write(stap, align='center')
    right(90)
    forward(10)
    pendown()

    for i in range(0,8):
        forward(10)
        penup()
        forward(10)
        pendown()
    penup()
    backward(170)
    left(90)
    forward(20)

ada = Turtle()
ada.color('red')
ada.shape('turtle')
ada.penup()
ada.goto(-160, 100)
ada.pendown()

bob = Turtle()
bob.color('blue')
bob.shape('turtle')
bob.penup()
bob.goto(-160, 70)
bob.pendown()

chris = Turtle()
chris.color('green')
chris.shape('turtle')
chris.penup()
chris.goto(-160, 40)
chris.pendown()

daisy = Turtle()
daisy.color('yellow')
daisy.shape('turtle')
daisy.penup()
daisy.goto(-160, 10)
daisy.pendown()

# We maken een lijstje van schildpadden
# Dit zorgt ervoor dat we dezelfde code niet meermaals moeten schrijven, maar gewoon een for lus kunnen gebruiken
turtles = [ada, bob, chris, daisy]

for turtle in turtles:
    for turn in range(0,20):
        turtle.right(18)

for draai in range(100):
    for turtle in turtles:
        turtle.forward(random.randint(1,5))


# Een trukje om je scherm nog even langer te tonen voordat het programma stopt
# Dit is niet nodig als je de online editor (trinket) gebruikt
import time
time.sleep(2)
