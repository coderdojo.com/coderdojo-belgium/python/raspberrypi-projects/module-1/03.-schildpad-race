# 03. Schildpad race

- [Racebaan](https://projects.raspberrypi.org/nl-NL/projects/turtle-race/2)
- [Uitdaging: meer lijnen](https://projects.raspberrypi.org/nl-NL/projects/turtle-race/3)
- [Raceschildpadden](https://projects.raspberrypi.org/nl-NL/projects/turtle-race/4)
- [Uitdaging: racetijd](https://projects.raspberrypi.org/nl-NL/projects/turtle-race/5)
- [Uitdaging: maak een draai](https://projects.raspberrypi.org/nl-NL/projects/turtle-race/6)
- [Uitdaging: stippelijnen](https://projects.raspberrypi.org/nl-NL/projects/turtle-race/7)
